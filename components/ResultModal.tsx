/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React, {
    useRef,
    useState
} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '@fortawesome/fontawesome-svg-core/styles.css'
import {
    faPaste,
    faTimes
} from '@fortawesome/free-solid-svg-icons'
import copy from 'copy-to-clipboard'

import SnackBar from '../components/SnackBar'

interface ResutlModalProps {
    encodedURL: string,
    targetLang: string,
    hide: boolean,
    hideHandler: (arg0: boolean) => void
}

const ResultModal: React.FC<ResutlModalProps> = ({ encodedURL, targetLang, hide, hideHandler }) => {
    const [isOpening, setIsOpening] = useState(true)
    const [copiedMsg, setCopiedMsg] = useState('')

    const modalContent = useRef<HTMLDivElement>()
    const modalAnimation = {
        open: 'animate__backInUp',
        close: 'animate__backOutDown'
    }

    const onAnimationCompleted = () => {
        if (modalContent != null && modalContent.current != null)
        {
            modalContent.current.classList.remove('animate__animated')
            if (isOpening)
            {
                modalContent.current.classList.remove(modalAnimation.open)
                setIsOpening(false)
            }
            else
            {
                modalContent.current.classList.remove(modalAnimation.close)
                setIsOpening(true)
                hideHandler(true)
            }
        }
    }

    const onCloseModal = () => {
        modalContent.current.classList.add('animate__animated', modalAnimation.close)
    }

    const onCopyLink = () => {
        copy(encodedURL)
        setCopiedMsg('Link copied to clipboard')
    }

    const resetCopiedMsg = () => {
        setCopiedMsg('')
    }

    const actionBtn = [
        {
            name: (
                <React.Fragment>
                    <FontAwesomeIcon icon={ faPaste }/> Copy Link
                </React.Fragment>
            ),
            action: () => onCopyLink()
        },
        {
            name: (
                <React.Fragment>
                    <FontAwesomeIcon icon={ faTimes }/> Close
                </React.Fragment>
            ),
            action: () => onCloseModal()
        }
    ]

    return hide ? null : (
        <div className="w3-modal" style={{ display: "block" }}>
            <div
                ref={ modalContent }
                className={ `w3-modal-content animate__animated animate__faster ${modalAnimation.open}` }
                onAnimationEnd={ onAnimationCompleted }
            >
                <div className="w3-container w3-padding">
                    <div id="modal-title">
                        <h3>Generated link</h3>
                    </div>
                    <div id="modal-body">
                        <div id="result-form">
                            <label>Spoiler wall link</label>
                            <input type="text" className="w3-input hide-outline" value={ encodedURL } readOnly/>
                        </div>

                        <div className="w3-right" style={{ marginTop: "12px" }}>
                            { actionBtn.map((action, index) => (
                                <span
                                    className="w3-button w3-round w3-blue"
                                    onClick={ action.action }
                                    key={ `action_${index}` }
                                    style={{
                                        fontWeight: 'bold',
                                        marginLeft: '3px',
                                        marginRight: '3px'
                                    }}
                                >
                                    { action.name }
                                </span>
                            )) }
                        </div>
                    </div>
                </div>
            </div>

            <SnackBar handReset={ resetCopiedMsg } msg={ copiedMsg }/>
        </div>
    )
}
export default ResultModal