/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React from 'react'
import Styles from '../styles/components/Footer.module.css'

const Footer: React.FC = () => {
    const year = (new Date).getFullYear()
    const links = {
        mpl: 'https://www.mozilla.org/en-US/MPL/2.0/',
        applab: 'https://kelvinchin12070811.gitlab.io/',
        prjPage: 'https://gitlab.com/kelvinchin12070811/spoiler-wall'
    }

    return (
        <footer className="w3-center w3-mobile">
            &copy;Copyright Kelvin Chin { year }, licensed under&nbsp;
            <a href={ links.mpl } className={ Styles.link }>
                MPL 2.0
            </a><br/>

            <a href={ links.prjPage } className={ Styles.link }>Celer Spoiler Wall</a> is an experiment of&nbsp;
            <a href={ links.applab } className={ Styles.link }>
                Kelvin's Application Laboratory
            </a>
        </footer>
    )
}
export default Footer