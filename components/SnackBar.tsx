/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React, {
    useRef
} from 'react'

interface SnackBarProps {
    msg: string,
    errorBar?: boolean,
    handReset: () => void
}

const SnackBar: React.FC<SnackBarProps> = ({ msg, errorBar, handReset }) => {
    const snackBarContainer = useRef<HTMLDivElement>()

    const snackBarAnimation = {
        open: 'animate__slideInDown',
        close: 'animate__slideOutUp'
    }

    const onAnimationDone = () => {
        if (snackBarContainer == null || snackBarContainer.current == null)
            return

        snackBarContainer.current.classList.remove('animate__animated')

        if (snackBarContainer.current.classList.contains(snackBarAnimation.open))
        {
            snackBarContainer.current.classList.remove(snackBarAnimation.open)
            setTimeout(() => {
                if (snackBarContainer == null || snackBarContainer.current == null)
                    return

                snackBarContainer.current.classList.add('animate__animated', snackBarAnimation.close)
            }, 5000)
        }
        else if (snackBarContainer.current.classList.contains(snackBarAnimation.close))
        {
            snackBarContainer.current.classList.remove(snackBarAnimation.close)
            handReset()
        }
    }

    if (msg === '') return null
    
    return (
        <div className="w3-display-topmiddle">
            <div
                className={ `animate__animated animate__fast ${snackBarAnimation.open}` }
                ref={ snackBarContainer }
                onAnimationEnd={ onAnimationDone }
            >
                <div
                    className={`
                        w3-tag
                        ${errorBar ? 'w3-red' : 'w3-green'}
                        w3-padding
                        w3-margin
                        w3-round
                    `}
                >
                    { msg }
                </div>
            </div>
        </div>
    )
}
export default SnackBar