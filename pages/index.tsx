/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import Footer from '../components/Footer'

import Styles from '../styles/index.module.css'

const Home: React.FC = () => {
    return (
        <React.Fragment>
            <Head>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/kelvinchin12070811/w3css/w3.min.css" />
                <title>Spoiler Wall</title>
            </Head>

            <div id="body">
                <h1 className="w3-grey w3-container" style={{ margin: "0" }}>Celer Spoiler Wall Temporary Home Page</h1>

                <div className="w3-margin"></div>

                <div
                    className="w3-card w3-padding w3-mobile"
                    style={{ width: "80%", marginLeft: "auto", marginRight: "auto" }}
                >
                    <ul className="w3-ul w3-hoverable">
                        <li className="w3-hover-white"><h3>Pages</h3></li>
                        <li><Link href={ '/builder' }><a className={ Styles.link }>Builder</a></Link></li>
                    </ul>
                </div>
            </div>

            <div className="w3-margin"></div>

            <Footer/>
        </React.Fragment>
    )
}

export default Home