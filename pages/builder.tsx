/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React, { useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import pako from 'pako'

import Footer from '../components/Footer'
import ResultModal from '../components/ResultModal'
import SnackBar from '../components/SnackBar'

const Builder: React.FC = () => {
    const router = useRouter()

    const metaTag = {
        desc: "Celer Spoiler Wall's link generation tool. Cover up a URL with spoiler staffs with Celer Spoiler Wall.",
        link: 'https://kelvinchin12070811.gitlab.io/spoiler-wall/builder',
        title: 'Spoiler Wall Link Builder'
    }
    const prefix = router.basePath
    const rgexURL =
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/


    const [url, setUrl] = useState('')
    const [encodedUrl, setEncodedUrl] = useState('')
    const [pageLang, setPageLang] = useState('')
    const [hideResultModal, setHideResultModal] = useState(true)
    const [snackMsg, setSnackMsg] = useState('')

    const onResetSnackBar = () => {
        setSnackMsg('')
    }

    const onBtnGenerateClicked = () => {
        if (url === '')
        {
            setSnackMsg('No URL provided')
        }
        else if (pageLang === '')
        {
            setSnackMsg('Please select a page language first')
        }
        else if (!rgexURL.test(url))
        {
            setSnackMsg('Valid URL with http(s) protocol is required')
        }
        else
        {
            if (snackMsg !== '') setSnackMsg('')

            let datUrl = url
            datUrl = Buffer.from(pako.deflate(url))
                .toString('base64')
                .replaceAll('+', '-')
                .replaceAll('/', '_')
                .replaceAll('=', '')
            let _encodedUrl = `${process.env.domain}/guarded`
            _encodedUrl += `/${pageLang}?dest=${datUrl}`
            setEncodedUrl(_encodedUrl)
            setHideResultModal(false)
        }
    }

    return (
        <React.Fragment>
            <Head>
                <title>Link Builder — Celer Spoiler Wall</title>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/kelvinchin12070811/w3css/w3.min.css"/>
                <link rel="stylesheet" href={`${prefix}/styles/builder.css`}/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
                <meta name="description" content={ metaTag.desc }/>
                <meta property="og:title" content={ metaTag.title }/>
                <meta property="og:description" content={ metaTag.desc }/>
                <meta property="og:url" content={ metaTag.link }/>
                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:title" content={ metaTag.title }/>
                <meta property="twitter:description" content={ metaTag.desc }/>
            </Head>
            <div id="body" className="w3-container">
                <h1>Spoiler Wall Link Builder</h1>
                
                <div className="w3-margin"></div>

                <div className="w3-card">
                    <div className="form-with-space">
                        <label htmlFor="url-input">URL</label>
                        <input
                            type="text"
                            id="url-input"
                            className="w3-input input-text"
                            placeholder="https://www.example.com"
                            value={ url }
                            onChange={ e => setUrl(e.target.value) }
                        />
                        
                        <div className="w3-margin"></div>

                        <label htmlFor="sel-lang">Page language</label>
                        <select
                            id="sel-lang"
                            className="w3-select w3-border-0 w3-border-bottom"
                            value={ pageLang }
                            onChange={ e => setPageLang(e.target.value) }
                        >
                            <option value="" disabled>Select language</option>
                            <option value="en">English</option>
                            <option value="zhtd">繁體中文</option>
                        </select>
                        
                        <div className="w3-margin"></div>
                        
                        <button
                            className="w3-button w3-blue w3-round-large"
                            onClick={ onBtnGenerateClicked }
                        >
                            Generate
                        </button>
                    </div>
                </div>

                <div className="w3-margin"></div>

                <Footer/>

                <ResultModal
                    encodedURL={ encodedUrl }
                    targetLang={ pageLang }
                    hide={ hideResultModal }
                    hideHandler={ setHideResultModal }
                />
                
                <SnackBar msg={ snackMsg } handReset={ onResetSnackBar } errorBar/>
            </div>
        </React.Fragment>
    )
}
export default Builder