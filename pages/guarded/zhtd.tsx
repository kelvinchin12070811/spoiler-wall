/***********************************************************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 **********************************************************************************************************************/
import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Router, useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faExclamationTriangle
} from '@fortawesome/free-solid-svg-icons'
import pako from 'pako'
import copy from 'copy-to-clipboard'

import Footer from '../../components/Footer'
import SnackBar from '../../components/SnackBar'

import '@fortawesome/fontawesome-svg-core/styles.css'

const ZHTD: React.FC<any> = ({ url }) => {
    const metas = {
        title: '劇透警告! — Celer Spoiler Wall',
        desc: '網頁含有劇透元素，可能會破會對相關作品的體驗',
        imgUrl: `${ process.env.domain }/images/banner-zhtd.png`
    }

    const router = useRouter()
    const prefix = router.basePath
    const [destUrl, setDestUrl] = useState('')
    const [showSnackBar, setShowSnackBar] = useState(false)


    useEffect(() => {
        if (router.query.dest !== undefined)
        {
            const destUrl: string = router.query.dest.toString()
            const decoded: string = pako.inflate(
                Buffer.from(destUrl
                    .replaceAll('-', '+')
                    .replaceAll('_', '/')
                , 'base64')
            , { to: 'string' })

            if (decoded == null)
                router.push('/404.html')

            setDestUrl(decoded)
        }

    }, [router.query])

    const onProceedBtnClicked = () => {
        if (!process.browser) return

        if (destUrl === '')
        {
            router.push('/404.html')
            return
        }

        window.location.href = destUrl
    }

    const onCopyOriLinkClicked = () => {
        copy(destUrl)
        setShowSnackBar(true)
    }

    return (
        <React.Fragment>
            <Head>
                <title>劇透警告—Celer Spoiler Wall</title>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/kelvinchin12070811/w3css/w3.min.css"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
                <link rel="stylesheet" href={ `${prefix}/styles/guarded/zhtd.css` }/>
                <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta property="og:title" content={ metas.title }/>
                <meta property="og:description" content={ metas.desc }/>
                <meta property="og:image" content={ metas.imgUrl }/>
                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:title" content={ metas.title }/>
                <meta property="twitter:description" content={ metas.desc }/>
                <meta property="twitter:image" content={ metas.imgUrl }/>
            </Head>
            <div id="body">
                <div id="container">
                    <div className="w3-mobile w3-center">
                        <h1>
                            <FontAwesomeIcon
                                icon={ faExclamationTriangle }
                                className="w3-text-red"
                                id="warningIco"
                            /> <br/>
                            網頁含有劇透元素
                        </h1>

                        <p>
                            該網頁含有對相關作品的劇透元素並可能影響體驗 <br/>
                            請三思而後行
                        </p>

                        <button
                            className="w3-button w3-blue w3-round-large"
                            onClick={ onProceedBtnClicked }
                        >
                            確定前往
                        </button>

                        <div style={{ margin: "40px" }}></div>

                        <p style={{ overflow: 'hidden' }}>
                            原始網址: <br/>
                            { destUrl } <br/>
                            <br/>
                            <a
                                onClick={ onCopyOriLinkClicked }
                                id="copy-ori-link"
                                className="w3-text-blue w3-text-hover-red"
                            >
                                複製原始鏈接
                            </a>
                        </p>
                    </div>
                    <div className="w3-margin" id="spacer"></div>
                    <Footer/>
                </div>
                
                <div id="snack-container">
                    <SnackBar
                        msg={ showSnackBar ? "已複製鏈接到剪貼板" : "" }
                        handReset={ () => setShowSnackBar(false) }
                    />
                </div>
            </div>
        </React.Fragment>
    )
}
export default ZHTD