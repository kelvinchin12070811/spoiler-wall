const { env } = require("process");
const { nodeModuleNameResolver } = require("typescript");

const _isProduction = process.env.NODE_ENV === 'production' ? true : false

module.exports = {
    assetPrefix: _isProduction ? '/spoiler-wall' : '',
    basePath: '/spoiler-wall',
    env: {
        isProduction: _isProduction,
        domain: _isProduction ?
            'https://kelvinchin12070811.gitlab.io/spoiler-wall':
            'http://localhost:3000/spoiler-wall',
    }
}